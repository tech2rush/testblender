#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/stitching.hpp>
#include <opencv2/stitching/warpers.hpp>
#include <opencv2/stitching/detail/blenders.hpp>

#include "testBlender.hpp"

int main (int argc, char** argv)
{

    std::cout << argc << std::endl;

    cv::Mat panorama, image1, image2;
    std::vector<cv::Mat> images;

    if (argc > 2)
    {
        image1 = cv::imread(argv[1]);
        image2 = cv::imread(argv[2]);
    }
    //Else load default images
    else
    {
    image1 = cv::imread("/home/jaran/hin/opencv_qt_test/stitching_test/test_leftside.png");
    image2 = cv::imread("/home/jaran/hin/opencv_qt_test/stitching_test/test_rightside.png");
    }

    //Resized images
    cv::Mat r_img1, r_img2;
    std::vector<cv::Mat> r_images;

    double scale = 0.25;
    if (argc > 3)
    {
        scale = atof(argv[3]);
    }
    std::cout << "Scaling by : " << scale << std::endl;

    cv::resize(image1, r_img1, cv::Size(), scale, scale, cv::INTER_NEAREST);
    cv::resize(image2, r_img2, cv::Size(), scale, scale, cv::INTER_NEAREST);

    double upscale = 4.0f;

    if (argc > 4)
    {
        upscale = atof(argv[4]);
    }

    std::cout << "Scaling by : " << upscale << std::endl;
    cv::resize(r_img1, r_img1, cv::Size(), upscale, upscale, cv::INTER_NEAREST);
    cv::resize(r_img2, r_img2, cv::Size(), upscale, upscale, cv::INTER_NEAREST);

    if (!image1.empty()) images.push_back(image1);
    if (!image2.empty()) images.push_back(image2);

    if (!r_img1.empty()) r_images.push_back(r_img1);
    if (!r_img2.empty()) r_images.push_back(r_img2);

    std::cout << "Images read: " << images.size() << std::endl;
    std::cout << argv[1] << std::endl;
    std::cout << argv[2] << std::endl;

    cv::Stitcher stitcher = cv::Stitcher::createDefault();


    cv::Stitcher::Status status = stitcher.estimateTransform(r_images);
    if (status!=cv::Stitcher::OK)
    {
        std::cout << "Error estimating transforms: " << int(status) << std::endl;
        return -1;
    }

    // Comment to desired blender.
    //cv::detail::MultiBandBlender blender(false, 5);
    //cv::detail::FeatherBlender blender;
    cv::detail::TestBlender blender;

    stitcher.setBlender(&blender);

    status = stitcher.composePanorama(panorama);

    if (status!=cv::Stitcher::OK)
    {
        std::cout << "Error composing: " << int(status) << std::endl;
        return -1;
    }


    cv::imshow("Panorama", panorama);
    cv::waitKey(0);

    return 0;
}
