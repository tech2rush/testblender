#ifndef TESTBLENDER_HPP
#define TESTBLENDER_HPP 

#include <opencv2/core.hpp>
#include <opencv2/stitching/detail/blenders.hpp>

namespace cv {
namespace detail {

//Modified feather blender, really - rewrite proper inheritance.
class CV_EXPORTS TestBlender : public Blender
{
public:
    TestBlender(double _sharpness = 0.02f);
    ~TestBlender();
    void prepare(Rect dst_roi);
    void feed(InputArray img, InputArray mask, Point tl);
    void blend(InputOutputArray dst, InputOutputArray dst_mask);

    Rect createWeightMaps(const std::vector<UMat> &masks, const std::vector<Point> &corners,
                          std::vector<UMat> &weight_maps);

    double getUpscale() const;
    void setUpscale(double value);

    float getSharpness() const;
    void setSharpness(float sharpness);

private:
    double upscale;//fjern
    float sharpness_;//Skriv bort

    UMat weight_map_;
    UMat dst_weight_map_;
};

} //namespace detail
} //namespace cv

#endif //TESTBLENDER_HPP 
